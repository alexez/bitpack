#include <iostream>
#include <typeindex>
#include <functional>
#include <cassert>
#include <vector>
#include <memory>
#include <limits>

template<bool B, class T = void>
using enable_if_t = typename std::enable_if<B, T>::type;

struct ResultBits {
};

class IConfig {
public:
    virtual ~IConfig() = default;

    virtual ResultBits pack() const = 0;
    virtual void unpack(const ResultBits &bits) = 0;
};

template<typename T>
class Config;

template<typename T, typename Enable = void>
class ConfigData;

template<typename T>
struct ConfigData<T, enable_if_t<std::is_integral<T>::value>> {
public:
    ConfigData(T min, T max)
        : min(min)
        , max(max) {}

    T min;
    T max;
};

template<typename T>
struct ConfigData<T, enable_if_t<std::is_floating_point<T>::value>> {
public:
    ConfigData(T min, T max)
        : min(min)
        , max(max) {}

    T min;
    T max;
};

struct CustomType {
    CustomType(uint16_t num, uint16_t denom)
        : numerator(num)
        , denominator(denom) { }

    uint16_t numerator;
    uint16_t denominator;
};

template<>
struct ConfigData<CustomType> {
public:
    ConfigData(CustomType lol, uint32_t kek)
        : lol(lol)
        , kek(kek) {}

    CustomType lol;
    uint32_t kek;
};

template<typename T>
class Config : public IConfig {
public:
    Config(T &ref, ConfigData<T> data)
        : _ref(ref)
        , _data(data) {}

    ResultBits pack() const override {
        return internalPack();
    }

    void unpack(const ResultBits &bits) override {
        return;
    }

private:
    template <typename Q = T, enable_if_t<std::is_integral<Q>::value, int> = 0>
    ResultBits internalPack() const {
        std::cout << "internalPack integral " <<  _data.min << " " << _data.max << std::endl;
    }

    template <typename Q = T, enable_if_t<std::is_floating_point<Q>::value, int> = 0>
    ResultBits internalPack() const {
        std::cout << "internalPack floating_point " << _data.min << " " << _data.max << std::endl;
    }

    template <typename Q = T, enable_if_t<std::is_same<Q, CustomType>::value, int> = 0>
    ResultBits internalPack() const {
        std::cout << "internalPack CustomType " << _data.lol.numerator << " " << _data.lol.denominator << " " << _data.kek << std::endl;
    }

    T &_ref;
    ConfigData<T> _data;
};

class Packer {
public:
    template<typename T, typename ...Args>
    void setItem(T &ref, Args... args) {
        std::cout << "setItem " << typeid(ref).name() << std::endl;
        m_configs.emplace_back(new Config<T>(ref, ConfigData<T>(args...)));
    }

    void pack() {
        for (const auto &config : m_configs) {
            auto result = config->pack();
            // some operation with result
        }
    }

private:
    std::vector<std::unique_ptr<IConfig>> m_configs;
};


//usage: 
int i = 3;
float f = 4.2f;
std::string s = "aaa";
CustomType ct = CustomType(2u, 4u);
Packer packer;
packer.setItem(f, 3.2f, 5.0f);
packer.setItem(i, 0, 8);
packer.setItem(ct, CustomType(5u, 2u), 876u);

// will fail 
// packer.setItem(s, std::string("a"), std::string("b"));

packer.pack();